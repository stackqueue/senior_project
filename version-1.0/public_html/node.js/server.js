/** 
 * This file is the node.js server. All files are linked to here so they can
 * run on node.js
 * 
 * @author Richard Cerone
 * @group stackqueue
 * @date 10/29/2015
 */

//Dependencies:
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var mysql = require('mysql'); //import mysql package.
var bodyParser = require('body-parser');
var credentials = require(__dirname + '/node/credentials.js');

//Here we are configuring express to use body-parser as middle-ware.
app.use(bodyParser.urlencoded({extended: false}));

//Setup cookies and sessions.
app.use(require('cookie-parser')(credentials.cookieSecret));
app.use(require('express-session')(
        {
            resave: false,
            saveUninitialized: false,
            secret: credentials.cookieSecret
        }));

var numTimers = 0;
var startTime; //To avoid another query to get the start time later on we will store it in a variable.

/***************************************************
 * All files to be included on the node.js server. *
 ***************************************************/

/***************************************************
 * HTML files.                                     *
 ***************************************************/
app.get('/', function (req, res) //Login page; loads by default.
{
    res.sendFile(__dirname + '/index.html');
});

app.get('/createUser', function (req, res)
{
    res.sendFile(__dirname + '/createUser.html');
});

app.get('/resultsHTML', function (req, res)
{
    res.sendFile(__dirname + '/results.html');
});

app.get('/manageEvents', function (req, res)
{
    res.sendFile(__dirname + '/manage_events.html');
});

app.get('/create', function (req, res)
{
    res.sendFile(__dirname + '/viewCreateEvent.html');
});

app.get('/timer', function (req, res)
{
    res.sendFile(__dirname + '/timer.html');
})

app.get('/onEventGUI', function (req, res)
{
    res.sendFile(__dirname + '/onEvent.html');
})
/***************************************************
 * CSS files.
 ***************************************************/
app.get('/animate', function (req, res) //CSS required for noty animations.
{
    res.sendFile(__dirname + '/css/Animate.css');
});

app.get('/jquery-ui-css', function (req, res)
{
    res.sendFile(__dirname + '/css/jquery-ui.css');
});

app.get('/timepicker-css', function (req, res)
{
    res.sendFile(__dirname + '/css/jquery-ui-timepicker-addon.css');
});

app.get('/bootstrapCSS', function (req, res)
{
    res.sendFile(__dirname + '/css/bootstrap.css');
});

app.get('/createCSS', function (req, res)
{
    res.sendFile(__dirname + '/css/create.css');
});

app.get('/loginCSS', function (req, res)
{
    res.sendFile(__dirname + '/css/login.css');
});

app.get('/manageCSS', function (req, res)
{
    res.sendFile(__dirname + '/css/manage.css');
});

app.get('/eventCSS', function (req, res)
{
    res.sendFile(__dirname + '/css/event.css');
});

app.get('/timerCSS', function (req, res)
{
    res.sendFile(__dirname + '/css/timer.css');
});

app.get('/resultsCSS', function (req, res)
{
    res.sendFile(__dirname + '/css/results.css');
});

/****************************************************
 * JavaScript Files.
 ****************************************************/
app.get('/hashPassword', function (req, res) //Does SHA-256 hashing for password.
{
    res.sendFile(__dirname + '/js/PasswordHash/hashPassword.js');
});

app.get('/getURLValues', function (req, res) //Grabs values from the URL.
{
    res.sendFile(__dirname + '/js/util/getURLValues.js');
});

app.get('/checkLogin', function (req, res)
{
    res.sendFile(__dirname + '/js/checkLogin.js');
});

app.get('/createNewUser', function (req, res) //Creates a new user.
{
    res.sendFile(__dirname + '/js/CreateUser/createUser.js');
});

app.get('/creationSuccess', function (req, res) //Shows success noty on login page.
{
    res.sendFile(__dirname + '/js/CreateUser/creationSuccess.js');
});

app.get('/results', function (req, res) //Shows success noty on login page.
{
    res.sendFile(__dirname + '/js/results.js');
});

app.get('/manage', function (req, res) //Shows success noty on login page.
{
    res.sendFile(__dirname + '/js/manage_events.js');
});

app.get('/viewCreate', function (req, res) //Shows success noty on login page.
{
    res.sendFile(__dirname + '/js/viewCreateEvent.js');
});

app.get('/cookies', function (req, res) //Shows success noty on login page.
{
    res.sendFile(__dirname + '/js/Cookies/cookies.js');
});
app.get('/createTimerID', function (req, res) //Does SHA-256 hashing for password.
{
    res.sendFile(__dirname + '/js/PasswordHash/createTimerID.js');
});
app.get('/timeConverter', function (req, res) //Does SHA-256 hashing for password.
{
    res.sendFile(__dirname + '/js/timeConversion/timeConverter.js');
});

app.get('/createTimer', function (req, res)
{
    res.sendFile(__dirname + '/js/timer/timer.js');
});

app.get('/logout', function (req, res)
{
    res.sendFile(__dirname + '/js/logout.js');
});

app.get('/onEvent', function (req, res)
{
    res.sendFile(__dirname + '/js/onEvent.js');
});

app.get('/resetTimer', function (req, res)
{
    res.sendFile(__dirname + '/js/timer/resetTimer.js');
});

app.get('/timerLogin', function (req, res) //Checks to see if code exists for event.
{
    res.sendFile(__dirname + '/js/timerLogin.js');
});

app.get('/getRaceData', function (req, res) //Loads race data on timer page.
{
    res.sendFile(__dirname + '/js/timer/getRaceData.js');
})

/****************************************************
 * JavaScript Packages.
 ****************************************************/
app.get('/sha256', function (req, res) //sha256 algorithm to hash password.
{
    res.sendFile(__dirname + '/js/js_sha/sha.js');
});


app.get('/noty', function (req, res) //noty used for popup notifications.
{
    res.sendFile(__dirname + '/js/noty/jquery.noty.packaged.min.js');
});

app.get('/jQuery', function (req, res) //noty used for popup notifications.
{
    res.sendFile(__dirname + '/js/jQuery/jquery-2.1.4.min.js');
});

app.get('/jspdf', function (req, res) //noty used for popup notifications.
{
    res.sendFile(__dirname + '/js/jspdf/dist/jspdf.debug.js');
});

app.get('/runner', function (req, res)
{
    res.sendFile(__dirname + '/js/runner/build/jquery.runner.js');
});

app.get('/jqueryUI', function (req, res)
{
    res.sendFile(__dirname + '/js/jQuery/jquery-ui/jquery-ui.js');
});

app.get('/timepicker', function (req, res)
{
    res.sendFile(__dirname + '/js/jQuery/jquery-ui-timepicker-addon.js');
});

app.get('/bootstrapJS', function (req, res)
{
    res.sendFile(__dirname + '/js/bootstrap/bootstrap.js');
});

/****************************************************
 * node.js files (besides server.js).
 ****************************************************/
app.get('/socket.io', function (req, res) //socket.io for real time data transfer.
{
    res.sendFile(__dirname + '/socket.io/socket.io.js');
});

app.get('/startTimer', function (req, res)
{
    res.sendFile(__dirname + '/js/timer/startTimer.js');
});

app.get('/stopTimer', function (req, res)
{
    res.sendFile(__dirname + '/js/timer/stopTimer.js');
});

app.get('/resetTimer', function (req, res)
{
    res.sendFile(__dirname + '/js/timer/resetTimer.js');
})

app.get('/adminStartTimers', function (req, res)
{
    res.sendFile(__dirname + '/js/timer/adminStartTimers.js');
});

app.get('/logTimes', function (req, res)
{
    res.sendFile(__dirname + '/js/timer/timeLog.js');
});

/****************************************************
 * mysql code.
 ****************************************************/
/**
 * createUser gets the user data in the JSON array sent via ajax from the 
 * createUser.js file. The server then queries the database to check for
 * a duplicate username before addding it. If the user name is not added 
 * we query the database again to insert the new account and return a boolean
 * variable. Otherwise we don't add the account, and return a false boolean 
 * value. It will throw an error if an issue occurs with the mySQL query.
 * 
 * @param {Object} req is data from the request body.
 * @param {Object} res is data that can be sent out from the server.
 */
app.post('/createUser', function (req, res)
{
    var username = req.body.user; //Get username from JSON array.
    var password = req.body.password; //Get password from JSON array.
    var obj; //This will be the result returned via JSON.

    //Open connection to the database.
    var connection = mysql.createConnection(
            {
                host: 'localhost', //host url
                user: 'root', //mysql username
                password: '', //mysql password
                database: 'stackqueue' //name of dbo
            });

    connection.connect(); //Open connection to database


    //Check to see if the username already exists.
    connection.query("SELECT * FROM accounts WHERE account_name = '" + username + "'", function (err, results)
    {
        if (!err) //Selection successful.
        {
            if (results.length > 0) //Username exists.
            {
                console.log('cannot add user.');
                obj = {done: false}; //Tell user he cannot use that name.
                res.send(obj); //Return false.
            } else if (results.length < 1) //Username does not exists.
            {
                var user = {account_name: username, password: password}; //Data we want  to insert.

                //Insert username and password into the database.
                connection.query('INSERT INTO accounts SET ?', user, function (err)
                {
                    if (!err) //Insert successful.
                    {
                        console.log('user added');
                        obj = {done: true}; //Return true.
                    } else //Internal error.
                    {
                        console.log('Error while performing Query1:\n ' + err);
                        obj = {done: 'error'}; //Return error.
                    }

                    res.send(obj); //Send return value back to page via JSON.
                });
            }
        } else //Internal error.
        {
            console.log('Error while performing Query:\n ' + err);
            obj = {done: 'error'};
            res.send(obj); //Send return value back to page via JSON.
        }

        connection.end(); //Close connection to the database.
    });
});

/**
 * login
 * 
 * @param {Object} req is data from the request body.
 * @param {Object} res is data that can be sent out from the server.
 */
app.post('/login', function (req, res)
{
    var username = req.body.user;
    var password = req.body.password;

    var connection = mysql.createConnection(
            {
                host: 'localhost', //host url
                user: 'root', //mysql username
                password: '', //mysql password
                database: 'stackqueue' //name of dbo
            });

    connection.connect();

    connection.query("SELECT * FROM accounts WHERE account_name = '" + username + "'", function (err, results)
    {
        if (!err)
        {
            if (results.length !== 0)
            {
                for (var i = 0; i < results.length; i++)
                {
                    if (results[i].account_name === username)
                    {
                        if (results[i].password === password)
                        {
                            req.session.accId = results[i].id;
                            req.session.username = username;

                            res.send({done: true});
                        } else
                        {
                            res.send({done: "badPassword"});
                        }
                    } else if (i === results.length - 1)
                    {
                        res.send({done: false});
                    }
                }
            } else
            {
                res.send({done: "empty"});
            }
        } else
        {
            res.send({done: "err"});
        }
    });

    connection.end();
});

/**
 * Checks to see if the event code exists before loggin in the timer.
 */
app.post('/verifyCode', function (req, res)
{
    var connection = mysql.createConnection(
            {
                host: 'localhost', //host url
                user: 'root', //mysql username
                password: '', //mysql password
                database: 'stackqueue' //name of dbo
            });

    connection.connect();

    var eventCode = req.body.code;

    connection.query("SELECT id FROM events WHERE timerID='" + eventCode + "';", function (err, results)
    {
        if (!err) //event code exists!
        {
            res.send({cleared: true, eventId: results[0].id});
        } else
        {
            res.send({cleared: false}); //Wrong event code.
        }
    });
});

/*
 *  logout simply destroys the current session and sends confirmation to the front end
 * 
 *  @param {Object} req is data from the request body.
 *  @param {Object} res is data that can be sent out from the server.
 */
app.post('/logout', function (req, res)
{
    req.session.destroy(function (err) {
        if (!err) {
            res.send({done: true});
        } else {
            res.send({done: false});
        }
    });
});

/*
 *  getAccId retrieves the account_id as specified in the session and it is sent to the front end
 * 
 *  @param {Object} req is data from the request body.
 *  @param {Object} res is data that can be sent out from the server.
 */
app.post('/getAccId', function (req, res)
{
    var accountId = req.session.accId; // current session's account id
    if (typeof accountId !== 'unknown' || accountId !== null)
    {
        res.send({accountId: accountId}); // send the account id to the front end
    } else
    {
        console.log("accountId is undefined.");
    }
});

/**
 * getEvents queries the database for all events that have the same account id. 
 * it then sends the array of events to the front end to be processed
 * 
 * @param {Object} req is data from the request body.
 * @param {Object} res is data that can be sent out from the server.
 */
app.post('/getEvents', function (req, res)
{
    var connection = mysql.createConnection(
            {
                host: 'localhost', //host url
                user: 'root', //mysql username
                password: '', //mysql password
                database: 'stackqueue' //name of dbo
            });

    connection.connect();

    connection.query("SELECT * FROM events WHERE account_id = " + req.session.accId + ";", function (err, events) {
        var event = events.length;
        if (!err) {
            if (event > 0) {
                res.send({events: events});
            }
        } else {
            console.log(err);
        }
    });
    connection.end();
});

/**
 * getTimerId queries the database for all timerId's from the event that has the same id. 
 * it then sends the array of events to the front end to be processed
 * 
 * @param {Object} req is data from the request body.
 * @param {Object} res is data that can be sent out from the server.
 */
app.post('/getTimerId', function (req, res)
{
    var connection = mysql.createConnection(
            {
                host: 'localhost', //host url
                user: 'root', //mysql username
                password: '', //mysql password
                database: 'stackqueue' //name of dbo
            });

    connection.connect();

    connection.query("SELECT timerID, total_par, num_lanes FROM events WHERE id = " + req.body.eventId + ";", function (err, results) {
        if (!err) {
            res.send({done: true, timerID: results[0].timerID, total_par: results[0].total_par, num_lanes: results[0].num_lanes});
        } else {
            res.send({done: false});
            console.log(err);
        }
    });

    connection.end();
});

/**
 * getParticipants queries the database for all participants in the event. 
 * it then sends the array of events to the front end to be processed
 * 
 * @param {Object} req is data from the request body.
 * @param {Object} res is data that can be sent out from the server.
 */
app.post('/getParticipants', function (req, res)
{
    var connection = mysql.createConnection(
            {
                host: 'localhost', //host url
                user: 'root', //mysql username
                password: '', //mysql password
                database: 'stackqueue' //name of dbo
            });


    connection.connect();

    connection.query("SELECT * FROM participants WHERE event_id = " + req.body.index + " AND has_timer = '0';", function (err, results) {
        if (!err) {
            res.send({done: true, results: results});
        }
        else
        {
            console.log(err);
            res.send({done: false});
        }
    });
    connection.end();
});

/**
 * getParticipants queries the database for all participants in the event. 
 * it then sends the array of events to the front end to be processed
 * 
 * @param {Object} req is data from the request body.
 * @param {Object} res is data that can be sent out from the server.
 */
app.post('/getResults', function (req, res)
{
    var connection = mysql.createConnection(
            {
                host: 'localhost', //host url
                user: 'root', //mysql username
                password: '', //mysql password
                database: 'stackqueue' //name of dbo
            });


    connection.connect();

    connection.query("SELECT first_name, last_name, elapsed_time FROM participants WHERE event_id = '" + req.body.eventId + "' ORDER BY elapsed_time asc;", function (err, results) {
        if (!err) {
            res.send({done: true, results: results});
        } else {
            res.send({done: false});
            console.log(err);
        }
    });

    connection.end();
});

/**
 * getRaceId queries the database for race_id of the race. 
 * it then sends the array of events to the front end to be processed
 * 
 * @param {Object} req is data from the request body.
 * @param {Object} res is data that can be sent out from the server.
 */
app.post('/getRaceId', function (req, res)
{
    var connection = mysql.createConnection(
            {
                host: 'localhost', //host url
                user: 'root', //mysql username
                password: '', //mysql password
                database: 'stackqueue' //name of dbo
            });

    connection.connect();

    connection.query("SELECT id FROM races WHERE event_id = " + req.body.id + ";", function (err, results) {
        if (!err) {
            res.send({results: results});
        } else {
            res.send({done: false});
            console.log(err);
        }
    });

    connection.end();
});

app.post("getLaneNumber", function (req, res) {
    var connection = mysql.createConnection(
            {
                host: 'localhost', //host url
                user: 'root', //mysql username
                password: '', //mysql password
                database: 'stackqueue' //name of dbo
            });
    connection.connect();
    connection.query("SELECT id, first_name, last_name FROM participants WHERE event_id = " + req.body.eventID + " has_timer = FALSE;", function (err, results) {
        if (!err) {
            res.send({done: true, results: results});
        } else {
            res.send({done: false});
        }
    });
});

/**
 * editEvent queries the database for the event with the corresponding id number. 
 * it then sends the array of event details to the front end to be processed
 * 
 * @param {Object} req is data from the request body.
 * @param {Object} res is data that can be sent out from the server.
 */
app.post('/editEvent', function (req, res)
{
    var connection = mysql.createConnection(
            {
                host: 'localhost', //host url
                user: 'root', //mysql username
                password: '', //mysql password
                database: 'stackqueue' //name of dbo
            });

    connection.connect();

    connection.query("SELECT event_name, total_par, num_lanes, date, time FROM events WHERE id = " + req.body.indexID + ";", function (err, events) {
        if (!err) {
            res.send({events: events}); // sends the event details
        } else {
            res.send({done: false});
        }
    });
    connection.end();
});

/**
 * delEvent queries the database for the event with the corresponding id number and
 * then it removes the event from the database
 * 
 * @param {Object} req is data from the request body.
 * @param {Object} res is data that can be sent out from the server.
 */
app.post('/delEvent', function (req, res)
{
    var connection = mysql.createConnection(
            {
                host: 'localhost', //host url
                user: 'root', //mysql username
                password: '', //mysql password
                database: 'stackqueue' //name of dbo
            });

    connection.connect();

    connection.query("DELETE FROM events WHERE id = " + req.body.rowID + ";", function (err) {
        if (!err) {
            connection.query("DELETE FROM participants WHERE event_id = '" + req.body.rowID + "';", function (err) {
                if (!err) {
                    res.send({done: "deleted"});
                } else {
                    console.log(err);
                    res.send({done: "noDelete"});
                }
            });
        } else {
            console.log(err);
        }
        connection.end();
    });
});

/**
 * updateEvent gets the new data from viewCreateEvent.html and updates the fields in the database
 * 
 * @param {Object} req is data from the request body.
 * @param {Object} res is data that can be sent out from the server.
 */
app.post('/updateEvent', function (req, res)
{
    var id = req.body.evId; //Get id from JSON array.
    var account = req.body.acc; //Get account from JSON array.
    var eventName = req.body.evName; //Get event name from JSON array.
    var numPar = req.body.numPart; //Get # of participants from JSON array.
    var numLanes = req.body.lanes; //Get # of lanes from JSON array.
    var date = req.body.evDate; //Get date from JSON array.
    var time = req.body.evTime; //Get time from JSON array.

    var eventDetails = {id: id, account_id: account, event_name: eventName, total_par: numPar, num_lanes: numLanes, date: date, time: time}; //data we want to insert

    var connection = mysql.createConnection(
            {
                host: 'localhost', //host url
                user: 'root', //mysql username
                password: '', //mysql password
                database: 'stackqueue' //name of dbo
            });

    connection.connect();

    connection.query("UPDATE events SET ? WHERE id=" + req.body.evId + ";", eventDetails, function (err) {
        if (!err) {
            res.send({done: true});
        } else {
            console.log(err);
            res.send({done: false});
        }
    });
    connection.end();
});

/**
 * updateParticipant 
 * 
 * @param {Object} req is data from the request body.
 * @param {Object} res is data that can be sent out from the server.
 */
app.post('/updateParticipant', function (req, res) {
    var connection = mysql.createConnection(
            {
                host: 'localhost', //host url
                user: 'root', //mysql username
                password: '', //mysql password
                database: 'stackqueue' //name of dbo
            });
    connection.connect();

    var parDetails = {first_name: req.body.firstName, last_name: req.body.lastName};

    connection.query("UPDATE participants SET ? WHERE event_id = '" + req.body.eventId + "' AND first_name = '" + req.body.oldFirst + "' AND last_name = '" + req.body.oldLast + "';", parDetails, function (err) {
        if (!err) {
            res.send({done: true});
        } else {
            console.log(err);
            res.send({done: false});
        }
    });
    connection.end();
});

/**
 * submitEvent gets the data from viewCreateEvent.html and a new event in the database
 * 
 * @param {Object} req is data from the request body.
 * @param {Object} res is data that can be sent out from the server.
 */
app.post('/submitEvent', function (req, res)
{
    var account = req.body.acc; //Get account from JSON array.
    var eventName = req.body.evName; //Get event name from JSON array.
    var numPar = req.body.numPart; //Get # of participants from JSON array.
    var numLanes = req.body.lanes; //Get # of lanes from JSON array.
    var date = req.body.evDate; //Get date from JSON array.
    var time = req.body.evTime; //Get time from JSON array.

    var eventDetails = {account_id: account, event_name: eventName, total_par: numPar, num_lanes: numLanes, date: date, time: time, num_timers: 0}; //data we want to insert

    var connection = mysql.createConnection(
            {
                host: 'localhost', //host url
                user: 'root', //mysql username
                password: '', //mysql password
                database: 'stackqueue' //name of dbo
            });

    connection.connect();

    connection.query('INSERT INTO events SET ?', eventDetails, function (err) {
        if (!err) {
            connection.query("SELECT id FROM events WHERE event_name = '" + eventName + "' AND total_par = '" + numPar + "' AND num_lanes = '" + numLanes + "' AND date = '" + date + "' AND time = '" + time + "';", function (err, result) {
                if (!err) {
                    res.send({result: result});
                } else {
                    console.log(err);
                    res.send({done: false});
                }
            });
            connection.end();
        } else {
            console.log(err);
            res.send({done: "notSubmitted"});
        }
    });
});

/**
 * submitEvent gets the data from viewCreateEvent.html and a new event in the database
 * 
 * @param {Object} req is data from the request body.
 * @param {Object} res is data that can be sent out from the server.
 */
app.post('/submitParticipant', function (req, res)
{
    var connection = mysql.createConnection(
            {
                host: 'localhost', //host url
                user: 'root', //mysql username
                password: '', //mysql password
                database: 'stackqueue' //name of dbo
            });

    var parDetails = {event_id: req.body.eventId, first_name: req.body.firstName, last_name: req.body.lastName};

    connection.connect();

    connection.query('INSERT INTO participants SET ?', parDetails, function (err) {
        if (!err) {
            res.send({done: true});
        } else {
            console.log(err);
            res.send({done: false});
        }
    });
    connection.end();
});

/**
 * createRaces creates a specified amount of races depending on the number of lanes
 * and the number of participants. races each have an event id that they correspond with
 * as well as it's own unique id.
 * 
 * @param {Object} req is data from the request body.
 * @param {Object} res is data that can be sent out from the server.
 */
app.post('/createRaces', function (req, res)
{
    var eventId = req.body.event; //Get account from JSON array.
    var numLanes = req.body.lanes; //Get event name from JSON array.
    var numPar = req.body.totalPar; //Get # of participants from JSON array.

    var raceDetails = {event_id: eventId, num_lanes: numLanes, num_par: numPar}; //data we want to insert

    var connection = mysql.createConnection(
            {
                host: 'localhost', //host url
                user: 'root', //mysql username
                password: '', //mysql password
                database: 'stackqueue' //name of dbo
            });

    connection.connect();

    connection.query('INSERT INTO races SET ?', raceDetails, function (err) {
        if (!err) {
            connection.query('SELECT id FROM races WHERE event_id=' + eventId, raceDetails, function (err, results) {
                if (!err) {
                    var lastId = results[results.length - 1].id;
                    res.send({id: lastId});
                } else {
                    console.log(err);
                    res.send({done: "error"});
                }
            });
        } else {
            console.log(err);
            res.send({done: false});
        }
        connection.end();
    });
});

/**
 * setParticipantRaceId
 * 
 * @param {Object} req is data from the request body.
 * @param {Object} res is data that can be sent out from the server.
 */
app.post('/setParticipantsTimer', function (req, res)
{
    var connection = mysql.createConnection(
            {
                host: 'localhost', //host url
                user: 'root', //mysql username
                password: '', //mysql password
                database: 'stackqueue' //name of dbo
            });

    connection.connect();

    connection.query("UPDATE participants SET has_timer = '1' WHERE id = '" + req.body.id + "';", function (err) {
        if (!err) {
            res.send({done: true});
        } else {
            console.log(err);
            res.send({done: false});
        }
    });
    connection.end();
});

/**
 * startEvent checks if the event within a certain timefram to start it
 * 
 * @param {Object} req is data from the request body.
 * @param {Object} res is data that can be sent out from the server.
 */
app.post('/startEvent', function (req, res) {
    var connection = mysql.createConnection(
            {
                host: 'localhost', //host url
                user: 'root', //mysql username
                password: '', //mysql password
                database: 'stackqueue' //name of dbo
            });
    connection.connect();
    connection.query("SELECT date, time FROM events where id =" + req.body.eventID + ";", function (err, result) {
        if (!err) {
            var totDate = result[0].date.toDateString() + ' ' + result[0].time + ' GMT-0500 (Eastern Standard Time)';
            var checkDate = Date.parse(totDate).valueOf();
            var now = Date.now().valueOf();
            if (Math.abs(checkDate - now) <= 1800000) {
                res.send({check: "startable"});
            } else {
                res.send({check: "unstartable"});
            }
        } else {
            res.send({check: "unstartable"});
        }
    });
    connection.end();
});

/**
 * setTimerID sets the timerId for a given event if it is unique
 * 
 * @param {Obj} req is the data from the request body
 * @param {Obj} res is the data sent out from the server
 */
app.post('/setTimerID', function (req, res) {
    var connection = mysql.createConnection(
            {
                host: 'localhost', //host url
                user: 'root', //mysql username
                password: '', //mysql password
                database: 'stackqueue' //name of dbo
            });
    connection.connect();
    connection.query("SELECT timerID FROM events WHERE timerID = '" + req.body.timerID + "';", function (err, results) {
        if (!err) {
            if (typeof results !== 'undefined') {
                for (var i = 0; i < results.length; i++) {
                    if (results[i].timer_id === req.body.timerID) {
                        res.send({check: "retry"});
                    }
                }
            }
            connection.query("UPDATE events SET timerID = '" + req.body.timerID + "' WHERE id = '" + req.body.eventID + "';", function (err) {
                if (!err) {
                    res.send({check: "start"});
                } else {
                    res.send({check: "retry"});
                }
            });
        } else {
            res.send({check: "retry"});
        }
        connection.end();
    });
});

/**
 * Destroys the session for logout.
 */
app.post('/destroySession', function (req, res)
{
    req.session.destroy();
    res.send({destroyed: true});
});

app.post('/getNextParticipant', function (req, res)
{
    var connection = mysql.createConnection(
            {
                host: 'localhost', //host url
                user: 'root', //mysql username
                password: '', //mysql password
                database: 'stackqueue' //name of dbo
            });
    connection.connect();

    connection.query("SELECT num_timers FROM events WHERE id='" + req.body.eventId + "';", function (err, results)
    {
        if (!err)
        {
            connection.query("SELECT first_name, last_name FROM participants WHERE id='" + req.body.eventId + "' AND has_timer=FALSE;", function (err, results2)
            {
                if (!err)
                {
                    var parData = [];
                    for (var i = 0; i < results[0].num_timers; i++)
                    {
                        parData.push(results2[i]); //Add participant data to array.
                    }

                    res.send({success: true, parData: parData});
                    connection.end();

                } else
                {
                    console.log(err);
                    res.send({success: false});
                }
            });
        } else
        {
            console.log(err);
            res.send({success: false});
        }
    });
});

app.post('/giveElapsedTime', function(req,res){
    var name = req.body.name.split(" ",2);
    console.log("first name: " + name[0]);
    console.log("last name: " + name[1]);
    console.log("elapsedTime: " + req.body.elapsedTime);
    var connection = mysql.createConnection(
            {
                host: 'localhost', //host url
                user: 'root', //mysql username
                password: '', //mysql password
                database: 'stackqueue' //name of dbo
            });
    connection.connect();
    connection.query("UPDATE participants SET elapsed_time='" + req.body.elapsedTime + "' WHERE first_name='" + name[0] + "' AND last_name='" + name[1] + "' AND event_id='" + req.body.eventID + "';", function(err){
        if(!err) {
            res.send({done: true});
        } else {
            console.log(err);
            res.send({done: false});
        }
    });
    connection.end();
});

/*********************************************************
 * Server-side socket.io code
 *********************************************************/
io.on('connection', function (socket)
{
    socket.on("joinEvent", function (data)
    {
        var connection = mysql.createConnection(
                {
                    host: 'localhost', //host url
                    user: 'root', //mysql username
                    password: '', //mysql password
                    database: 'stackqueue' //name of dbo
                });
        connection.connect();

        connection.query("SELECT id, num_timers, num_lanes FROM events WHERE timerID='" + data.timerId + "';", function (err, results)
        {
            if (!err)
            {
                if (results[0].num_timers < results[0].num_lanes)
                {
                    var numTimers = results[0].num_timers + 1; //Increment timer amount.
                    connection.query("UPDATE events SET num_timers='" + numTimers + "' WHERE id='" + results[0].id + "';", function (err)
                    {
                        if (!err)
                        {
                            io.sockets.emit("eventJoined", {joined: true, timerNumber: numTimers, eventId: results[0].id, timerId: data.timerId}); //Successfully joined event.
                        } else //Too many timers or error.
                        {
                            console.log(err);
                            io.emit("eventJoined", {joined: false});
                        }
                        connection.end();
                    });
                }
            }
        });
    });

    //Send start sequence to timers.
    socket.on('startRace', function (data)
    {
        if (data.start === true)
        {
            io.sockets.emit('startTimer', {start: data.start, eventId: data.eventId, timerID: data.timerID});
            
            for (var i = 0; i < data.racers.length; i++)
            {
                var connection = mysql.createConnection(
                        {
                            host: 'localhost', //host url
                            user: 'root', //mysql username
                            password: '', //mysql password
                            database: 'stackqueue' //name of dbo
                        });

                connection.connect();

                var name = data.racers[0].split(" ", 2);
                var firstName = name[0];
                var lastName = name[1];
                
                startTime = data.startTime; //assign startTime for later.
                console.log("In start race: " + startTime);
                
                connection.query("UPDATE participants SET start_time='" + data.startTime + "' WHERE first_name='" + firstName + "' AND last_name='" + lastName + "' AND event_id = '" + data.eventId + "';", function (err) {
                    if (!err) {
                        socket.emit('startTimeLogged', {logged: true});
                    } else {
                        console.log(err);
                        socket.emit('startTimeLogged', {logged: false});
                    }
                });
            }
            connection.end();
        }
    });

    //Stop the timer.
    socket.on('giveStopTime', function (data)
    {
        console.log("stop time");
        if (data.stopped === true)
        {
            var connection = mysql.createConnection(
                    {
                        host: 'localhost', //host url
                        user: 'root', //mysql username
                        password: '', //mysql password
                        database: 'stackqueue' //name of dbo
                    });
            connection.connect();

            date = Date.now();

            var firstName = data.firstName;
            var lastName = data.lastName;
            var fullName = firstName + " " + lastName;

            connection.query("UPDATE participants SET stop_time='" + data.stopTime + "' WHERE first_name='" + firstName + "' AND last_name='" + lastName + "' AND has_timer='0' AND event_id ='" + data.eventId + "';", function (err)
            {
                if (!err) //Stop time inserted.
                {
                    console.log("In stop time: " + startTime);
                   //Tell timer stop time was logged.
                   socket.emit("stopTimeLogged", {logged: true, eventId: data.eventId, timerID: data.timerID, name: fullName, laneNumber: data.laneNumber}); 
                   //Send to the admin to calculate elapsed time.
                   io.emit('calculateElapsed', {startTime: startTime, stopTime: data.stopTime, eventId: data.eventId, timerId: data.timerID, name: fullName, laneNumber: data.laneNumber});
                } else //Stop time not inserted.
                {
                    console.log(err);
                    socket.emit("stopTimeLogged", {logged: false});
                }
            });
            connection.end();
        }
    });

    //Delete all times from reset.
    socket.on("resetTime", function (data)
    {
        var connection = mysql.createConnection(
                {
                    host: 'localhost', //host url
                    user: 'root', //mysql username
                    password: '', //mysql password
                    database: 'stackqueue' //name of dbo
                });
        connection.connect();

        connection.query("UPDATE participants SET start_time='',  stop_time='' WHERE id='" + data.parID + "';", function (err)
        {
            if (!err)
            {
                io.sockets.emit("timeReset", {resetLogged: true});
            } else
            {
                io.sockets.emit("timeReset", {resetLogged: false});
            }
        });
        connection.end();
    });

    socket.on("newParticipants", function (data)
    {
        console.log(data.racers);
        io.sockets.emit("newResults", {gotNew: true, eventID: data.eventID, timerId: data.timerID, results: data.racers});
    });
});

/***************************************************
 * End of files to be included.
 ***************************************************/

//Listening at this port:
http.listen(3000, function ()
{
    console.log('listening on *:3000');
});

/* Can We use this instead?
 app.set('port', (process.env.PORT || 5000));
 
 app.listen(app.get('port'), function() {
 console.log('Node app is running on port', app.get('port'));
 });
 */
