/* 
 @author: Nick Mariani
 @team: stackqueue
 @date: 10/29/15
 @class: ViewCreateEvent.js
 */

var name = document.getElementById('evName');
var numLanes = document.getElementById('totLanes').value;
var numParticipants = document.getElementById('totParticipants');
var date = document.getElementById('date');
var time = document.getElementById('time');
var editting = false;
var pararr;

/**
 *  when this page loads, if the page has an id already, then it will retrieve
 *  the cookie and place the corresponding details into the appropriate fields
 *  
 * @returns {undefined}
 */
this.onload = function () {
    if (window.location.href.indexOf("eventId=") > -1) { // check to see if the user is editting an event
        var data = getCookie("event"); // retrieve the cookie
        var participants = getCookie("participants");
        var arr = data.split(",", 5); // split the cookie at commas into an array 
        document.getElementById('evName').value = arr[0]; // set the name text field
        document.getElementById('totParticipants').value = arr[1]; // set the participants text field
        document.getElementById('totLanes').value = arr[2]; //set the lanes text field
        document.getElementById('date').value = arr[3].substring(0, 10); // set the date text field with only first 10 characters
        document.getElementById('time').value = arr[4]; // set the time text field
        pararr = participants.split(",", arr[1]);
        createParticipants();
        for (var i = 0; i < arr[1]; i++) {
            document.getElementById("participant" + i).value = pararr[i];
        }
        editting = true; // this event is being editted

        var div = document.getElementById('create_edit');
        var header = document.createElement('h1');
        var text = document.createTextNode("Edit Event");
        header.appendChild(text);
        div.appendChild(header);
    }
    else
    {
        var div = document.getElementById('create_edit');
        var header = document.createElement('h1');
        var text = document.createTextNode("Create Event");
        header.appendChild(text);
        div.appendChild(header);
    }
};

// assigns the datepicker to this button when clicked
document.getElementById('date').addEventListener("click", function () {
    $("#date").datetimepicker({
        dateFormat: "yy-mm-dd",
        altField: "#time"
    });
    $("#date").datepicker('show');
});

// assigns the submit() function to this button when clicked
document.getElementById('add').addEventListener("click", function () {
    submit();
});

/**
 * Checks to see if a value is a number
 * 
 * @param {type} value
 * @returns {Boolean}
 */
function isInt(value) {
    return value % 1 === 0;
}

/**
 *  when submit is clicked, this function will either create a new event or
 *  update the event depending on whether it is being editted or not. it retrieves 
 *  the account id from the server with an AJAX request and within that, it calls another
 *  AJAX post which sends all of the event's info to the database
 *  
 * @returns {undefined}
 */
function submit() {
    var eventName = document.getElementById('evName').value;
    var numLanes = document.getElementById('totLanes').value;
    var numPar = document.getElementById('totParticipants').value;
    var eventDate = document.getElementById('date').value;
    var eventTime = document.getElementById('time').value;
    var participantsList = getEnteredParticipants();
    var values;
    var type;
    var correctNames;

    if (eventName !== "") {
        if (numLanes !== "") {
            if (numPar !== "") {
                if (eventDate !== "") {
                    if (eventTime !== "") {
                        for (var i = 0; i < numPar; i++) {
                            if (values !== false) {
                                if (document.getElementById("participant" + i).value !== "") {
                                    values = true;
                                }
                                else {
                                    values = false;
                                }
                            }
                        }
                    }
                    else
                        values = false;
                }
                else
                    values = false;
            }
            else
                values = false;
        }
        else
            values = false;
    }
    else
        values = false;

    if (values === false) {
        var n = noty(
                {
                    layout: 'topRight',
                    theme: 'relax',
                    type: 'error',
                    text: 'Please fill in the missing fields.',
                    animation:
                            {
                                open: 'animated fadeInRightBig',
                                close: 'animated fadeOutLeft'
                            },
                    maxVisible: 5,
                    closeWith: ['click'],
                    timeout: 2500, //2500ms
                    buttons: false
                });
    }

    if (isInt(numLanes) && isInt(numPar)) {
        for (var i = 0; i < numPar; i++) {
            if (correctNames !== false) {
                var par = participantsList[i].split(" ", 2);
                if (par.length !== 2) {
                    correctNames = false;
                }
            }
        }
    }
    else {
        var n = noty(
                {
                    layout: 'topRight',
                    theme: 'relax',
                    type: 'error',
                    text: 'Please make sure that "Number of Participants" and "Number of Lanes" are numbers.',
                    animation:
                            {
                                open: 'animated fadeInRightBig',
                                close: 'animated fadeOutLeft'
                            },
                    maxVisible: 5,
                    closeWith: ['click'],
                    timeout: 2500, //2500ms
                    buttons: false
                });
        type = false;
    }

    if (correctNames === false) {
        var n = noty(
                {
                    layout: 'topRight',
                    theme: 'relax',
                    type: 'error',
                    text: 'All participants must have a first and last name.',
                    animation:
                            {
                                open: 'animated fadeInRightBig',
                                close: 'animated fadeOutLeft'
                            },
                    maxVisible: 5,
                    closeWith: ['click'],
                    timeout: 2500, //2500ms
                    buttons: false
                });
    }

    if (values !== false && type !== false && correctNames !== false) {

        if (editting !== true) {
            $.post('/getAccId', {}, function (data) {
                var account = data.accountId;

                $.post('/submitEvent', {acc: account, evName: eventName, numPart: numPar, lanes: numLanes, evDate: eventDate, evTime: eventTime}, function (data) {
                    if (data.done !== "notSubmitted") {
                        console.log(data.result[0].id);
                        for (var i = 0; i < numPar; i++) {
                            var par = participantsList[i].split(" ", 2);
                            $.post('/submitParticipant', {eventId: data.result[0].id, firstName: par[0], lastName: par[1]}, function (data2) {
                                if (data2.done !== false) {
                                    window.location = "/manageEvents";
                                }
                            });
                        }
                    }
                    else {
                        var n = noty(
                                {
                                    layout: 'topRight',
                                    theme: 'relax',
                                    type: 'error',
                                    text: 'Event could not be submitted',
                                    animation:
                                            {
                                                open: 'animated fadeInRightBig',
                                                close: 'animated fadeOutLeft'
                                            },
                                    maxVisible: 5,
                                    closeWith: ['click'],
                                    timeout: 2500, //2500ms
                                    buttons: false
                                });
                    }
                });
            });
        }
        else {
            var id;
            if (window.location.href.indexOf("eventId=") > -1) {
                var url = document.URL;
                id = url.substring(url.lastIndexOf("=") + 1); // get the event id from the url
            }
            $.post('/getAccId', {}, function (data) {
                var account = data.accountId;
                $.post('/updateEvent', {evId: id, acc: account, evName: eventName, numPart: numPar, lanes: numLanes, evDate: eventDate, evTime: eventTime}, function (data) {
                    if (data.done === true) {
                        for (var i = 0; i < numPar; i++) {
                            var par = participantsList[i].split(" ", 2);
                            var oldPar = pararr[i].split(" ", 2);
                            $.post('/updateParticipant', {eventId: id, firstName: par[0], lastName: par[1], oldFirst: oldPar[0], oldLast: oldPar[1]}, function (data2) {
                                if (data2.done === true) {
                                    window.location = "/manageEvents";
                                }
                                else {
                                    var n = noty(
                                            {
                                                layout: 'topRight',
                                                theme: 'relax',
                                                type: 'error',
                                                text: 'Event Participants could not be updated',
                                                animation:
                                                        {
                                                            open: 'animated fadeInRightBig',
                                                            close: 'animated fadeOutLeft'
                                                        },
                                                maxVisible: 5,
                                                closeWith: ['click'],
                                                timeout: 2500, //2500ms
                                                buttons: false
                                            });
                                }
                            });
                        }
                    }
                    else {
                        var n = noty(
                                {
                                    layout: 'topRight',
                                    theme: 'relax',
                                    type: 'error',
                                    text: 'Event could not be updated',
                                    animation:
                                            {
                                                open: 'animated fadeInRightBig',
                                                close: 'animated fadeOutLeft'
                                            },
                                    maxVisible: 5,
                                    closeWith: ['click'],
                                    timeout: 2500, //2500ms
                                    buttons: false
                                });
                    }
                });
            });
        }
    }
}


// assigns the cancel() function to this button when clicked
document.getElementById('cancel').addEventListener("click", function () {
    cancel();
});

/**
 *  cancels the creation or update of the event
 *  
 * @returns {undefined}
 */
function cancel() {
    editting = false;
    window.location = "/manageEvents";
}

document.getElementById('totParticipants').addEventListener("change", function () {
    createParticipants();
});

function createParticipants() {
    var div = document.getElementById('participants');
    div.innerHTML = '';
    var numPar = document.getElementById('totParticipants').value;
    for (var i = 0; i < numPar; i++) {
        div.innerHTML += "<div class='row'><div class='col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xs-offset-5 col-sm-offset-5 col-md-offset-5 col-lg-offset-5'>\n\
                            Participant " + (i + 1) + ":</div></div>";
        
        div.innerHTML += "<div class='row'><div class='col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xs-offset-5 col-sm-offset-5 col-md-offset-5 col-lg-offset-5'>\n\
                            <div class='form-group'><input type='text' class='form-control' id='participant" + i + "'></div></div></div>";
    }
}

function getEnteredParticipants() {
    var numPar = document.getElementById("totParticipants").value;
    var newParArr = [];
    for (var i = 0; i < numPar; i++) {
        newParArr.push(document.getElementById("participant" + i).value);
    }
    return newParArr;
}