/**
 * This sends the signal to start the timers from the Admin.
 * 
 * @author Richard Cerone
 * @team stackqueue
 * @date 11/10/2015
 */

var socket = io('http://localhost:3000'); //Create socket.

if(socket.connected === false)
{
    var socket = io('http://localhost:3000');
}

var id;
var timerID;

this.onload = function () {
    if (window.location.href.indexOf("eventId=") > -1) {
        var url = document.URL;
        id = url.substring(url.lastIndexOf("=") + 1); // get the event id from the url
        getTimerID();
    }
};

function getTimerID() {
    $.post('/getTimerId', {eventId: id}, function (data) {
        if (data.done !== false) {
            timerID = data.timerID;
        }
    });
}

/**
 * Starts all the timers in the room.
 */
function startTimers()
{
    var date = Date.now();
    socket.emit('startRace', {start: true, eventId: id, timerID: timerID, racers: timerRacers, startTime: date}); //Tell server race is starting.
}

var start = document.getElementById('startTimers');

if (start.addEventListener)
{
    start.addEventListener('click', startTimers, false);
}
