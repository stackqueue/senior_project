/**
 * Starts the timer and sends data to the server.
 * 
 * @author Richard Cerone
 * @team stackqueue
 * @date 11/10/2015
 */

var cookie = getCookie("timerCookie").split(",", 5); //get cookie.
/**
 * Listen for the admin to approve the start of all timers.
 * 
 * @param {String} name of the 'command' being sent out.
 * @param {Boolean} A boolean telling the timer to start.
 */
socket.on('startTimer', function (data)
{
    if (data.start === true && data.eventId == cookie[0] && data.timerID == cookie[2])
    {
        $('#runner').runner('start');
    }
    else
    {
        console.log("failed");
    }
});