/**
 * Gets the data needed for timer for the race it is timing.
 * 
 * @author Richard Cerone
 * @team stackqueue
 * @date 11/12/2015
 */

var socket = io("http://localhost:3000");

var cookie = getCookie("timerCookie").split(",", 5); //get cookie.
console.log(cookie);

//Load lane number.
var lane = document.getElementById("lane");
lane.innerHTML += " " + cookie[1];

if(cookie[3] !== ""){
    var name = document.getElementById("name");
    name.innerHTML += cookie[3];
}


/**
 * Gets participants for the race.
 * 
 * 
 * @param {type} param1
 * @param {type} param2
 */
socket.on("newResults", function (data)
{
    var timerNumber = cookie[1];
    var timerId = cookie[2];
    var parData = data.results;

    if (data.timerId == timerId && data.eventID == cookie[0])
    {
        var parName = parData[timerNumber - 1];

        var name = document.getElementById("name");
        name.innerHTML = "Participant Name: " + parName;
        
        //newCookie is eventID, timerNumber, timerID, firstName, lastName
        var newCookie = [cookie[0], cookie[1], cookie[2], parName, '']; //Add data to new cookie.
        var expires = getExpDate(0, 0, 30); //Expire time for new cookie.
        setCookie("timerCookie", newCookie, expires); //Create new cookie.
    }
});