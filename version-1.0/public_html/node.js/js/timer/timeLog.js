/**
 * Gives confirmation of succesful time insertions or time insertion failures 
 * from the server side to the user.
 * 
 * @author Richard Cerone
 * @team stackqueue
 * @date 11/12/2015
 */

var socket = io.connect('http://localhost:3000');

if(socket.connected === false)
{
    var socket = io('http://localhost:3000');
}

/**
 * Lets the timer know if their start time was recorded or not.
 */
socket.on('startTimeLogged', function(data)
{
    if(data.logged === true) //Start time was logged.
    {
        var n = noty(
        {
            layout: 'topRight',
            theme: 'relax',
            type: 'success',
            text: 'Start time logged',
            animation:
            {
                open: 'animated fadeInRightBig',
                close: 'animated fadeOutLeft'
            },
            maxVisible: 5,
            closeWith: ['click'],
            timeout: 2500,    //2500ms
            buttons: false
        });
    }
    else //Time not logged.
    {
        var n = noty(
        {
            layout: 'topRight',
            theme: 'relax',
            type: 'error',
            text: 'Time was not recorded. Please tell admin to void race results.',
            animation:
            {
                open: 'animated fadeInRightBig',
                close: 'animated fadeOutLeft'
            },
            maxVisible: 5,
            closeWith: ['click'],
            timeout: 2500,    //2500ms
            buttons: false
        });
    }
});

/**
 * Lets the timer know if their stop time was recorded or not.
 */
socket.on("stopTimeLogged", function(data)
{
   if(data.logged === true) //Stop time was logged.
   {
        var n = noty(
        {
            layout: 'topRight',
            theme: 'relax',
            type: 'success',
            text: 'Stop time logged',
            animation:
            {
                open: 'animated fadeInRightBig',
                close: 'animated fadeOutLeft'
            },
            maxVisible: 5,
            closeWith: ['click'],
            timeout: 2500,    //2500ms
            buttons: false
        });
   }
   else //Stop time was not logged.
   {
        var n = noty(
        {
            layout: 'topRight',
            theme: 'relax',
            type: 'error',
            text: 'Time was not recorded. Please tell admin to void race results.',
            animation:
            {
                open: 'animated fadeInRightBig',
                close: 'animated fadeOutLeft'
            },
            maxVisible: 5,
            closeWith: ['click'],
            timeout: 2500,    //2500ms
            buttons: false
        });
   }
});

socket.on("timeReset", function(data)
{
   if(data.resetLogged === true) //Time was reset on server.
   {
        var n = noty(
        {
            layout: 'topRight',
            theme: 'relax',
            type: 'success',
            text: 'Time was reset',
            animation:
            {
                open: 'animated fadeInRightBig',
                close: 'animated fadeOutLeft'
            },
            maxVisible: 5,
            closeWith: ['click'],
            timeout: 2500,    //2500ms
            buttons: false
        });
   }
   else
   {
        var n = noty(
        {
            layout: 'topRight',
            theme: 'relax',
            type: 'error',
            text: 'Time could not be reset',
            animation:
            {
                open: 'animated fadeInRightBig',
                close: 'animated fadeOutLeft'
            },
            maxVisible: 5,
            closeWith: ['click'],
            timeout: 2500,    //2500ms
            buttons: false
        });
   }
});
    


