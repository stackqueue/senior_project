/**
 * Stop the instance of this user's timer and send that
 * time to the server.
 * 
 * @author Richard Cerone
 * @team stackqueue
 * @date 11/10/2015
 */

var socket = io.connect('http://localhost:3000');

if(socket.connected === false)
{
    socket = io('http://localhost:3000');
}

var cookie = getCookie("timerCookie").split(",", 5); //get cookie.

/**
 * Tell server the timer stopped and stop the timer. Send stop time to the
 * server.
 */
function stopTime()
{
    $('#runner').runner('stop'); //Stop timer
    
    var date = Date.now(); //Get stop time.

    var participantName = document.getElementById("name").innerHTML;
    var name = participantName.split(" ", 4);
    var firstName = name[2]; //Get first name from split string.
    var lastName = name[3]; //Get last name from split string.
    
    socket.emit('giveStopTime', {stopped: true, firstName: firstName, lastName: lastName, eventId: cookie[0], timerID: cookie[2], laneNumber: cookie[1], stopTime: date}); //Tell server time was stopped.
    
    console.log("stopped");
}

var stop = document.getElementById('stop');

if (stop.addEventListener)
{
    stop.addEventListener('click', stopTime, false);
}