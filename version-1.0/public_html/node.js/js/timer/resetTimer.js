/**
 * Resets the timer for next race or redo.
 * 
 * @author Richard Cerone
 * @team stackqueue
 * @date 11/10/2015
 */

var socket = io("http://localhost:3000");

/**
 * Resets the timer to 0.
 */
function resetTimer()
{
    $('#runner').runner('reset');
    
    var cookie = getCookie("timerCookie");
    var participant = cookie.split(",", 5);
    
    socket.emit("resetTime", {reset: true, parID: participant[0]}); //Tell server to delete all times for participant.
}

var reset = document.getElementById('reset');

if(reset.addEventListener)
{
    reset.addEventListener('click', resetTimer, false);
}