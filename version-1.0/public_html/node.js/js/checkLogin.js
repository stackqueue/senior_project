/**
 * Check the user credentials in the database and make sure the username and 
 * password match.
 * 
 * @author Richard Cerone
 * @group stackqueue
 * @date 11/03/2015
 */

/**
 * Let the user login and check if there username and password match.
 * If they do redirect to manage_events.html.
 * 
 * @returns {Boolean | String} true if the login was successful. False if the
 * username entered was wrong. 'badPassword' if the password does not match.
 * 'empty' if database has no users. 'error' if there was an error querying the
 * database.
 */
function login()
{
    var username = document.getElementById('admin').value;
    var password = document.getElementById('adminPassword').value;
    
    if(username !== '')
    {
        password = sha(password);
        
        if(password !== true)
        {
            $.post('/login', {user: username, password: password}, function(data)
            {
                if(data.done === true) //Success.
                {
                    window.location = "/manageEvents";
                }
                else if(data.done === false) //Wrong username.
                {
                    var n = noty(
                    {
                        layout: 'topRight',
                        theme: 'relax',
                        type: 'error',
                        text: 'Username not found, please try again.',
                        animation:
                        {
                            open: 'animated fadeInRightBig',
                            close: 'animated fadeOutLeft'
                        },
                        maxVisible: 5,
                        closeWith: ['click'],
                        timeout: 2500,    //2500ms
                        buttons: false
                    });
                }
                else if(data.done === 'badPassword') //Wrong password.
                {
                    var n = noty(
                    {
                        layout: 'topRight',
                        theme: 'relax',
                        type: 'error',
                        text: 'The password entered was incorrect, please try again.',
                        animation:
                        {
                            open: 'animated fadeInRightBig',
                            close: 'animated fadeOutLeft'
                        },
                        maxVisible: 5,
                        closeWith: ['click'],
                        timeout: 2500,    //2500ms
                        buttons: false
                    });
                }
                else if(data.done === 'empty') //empty database.
                {
                    var n = noty(
                    {
                        layout: 'topRight',
                        theme: 'relax',
                        type: 'error',
                        text: 'Database is empty.',
                        animation:
                        {
                            open: 'animated fadeInRightBig',
                            close: 'animated fadeOutLeft'
                        },
                        maxVisible: 5,
                        closeWith: ['click'],
                        timeout: 2500,    //2500ms
                        buttons: false
                    });
                }
                else if(data.done === 'err') //Database error.
                {
                    var n = noty(
                    {
                        layout: 'topRight',
                        theme: 'relax',
                        type: 'error',
                        text: 'Sorry, there was an error with the server.',
                        animation:
                        {
                            open: 'animated fadeInRightBig',
                            close: 'animated fadeOutLeft'
                        },
                        maxVisible: 5,
                        closeWith: ['click'],
                        timeout: 2500,    //2500ms
                        buttons: false
                    });
                }
                else
                {
                    var n = noty(
                    {
                        layout: 'topRight',
                        theme: 'relax',
                        type: 'error',
                        text: 'Unknown error :(',
                        animation:
                        {
                            open: 'animated fadeInRightBig',
                            close: 'animated fadeOutLeft'
                        },
                        maxVisible: 5,
                        closeWith: ['click'],
                        timeout: 2500,    //2500ms
                        buttons: false
                    });
                }
            });
        }
    }
}

var button = document.getElementById('adminLogin');

if(button.addEventListener)
{
    button.addEventListener('click', login, false);
}



