/* 
 @author: Nick Mariani
 @team: stackqueue
 @date: 10/29/15
 @class: results.js
 */

this.onload = function () {
    var id = getURLValue("eventId");
    $.post('/getResults', {eventId: id}, function (data) {
        for (var i = 0; i < data.results.length; i++) {
            console.log("results: " + data.results[i].elapsed_time);
            generateTable(i, data.results[i]);
        }
    });
};

function generateTable(i, data){
    var table = document.getElementById("resultsBody");
    var row = table.insertRow(i);
    var placeCell = row.insertCell(0);
    var nameCell = row.insertCell(1);
    var timeCell = row.insertCell(2);
    placeCell.innerHTML = i + 1;
    nameCell.innerHTML = data.first_name + " " + data.last_name;
    timeCell.innerHTML = data.elapsed_time;
}

var button = document.getElementById('save');

if (button.addEventListener) {
    button.addEventListener('click', savePDF, false);
}

function savePDF() {

    var pdf = new jsPDF('p', 'pt', 'letter');
    source = $('#content')[0];

    specialElementHandlers = {
        '#donothing': function (element, renderer) {
            return true;
        }
    };
    margins = {
        top: 80,
        bottom: 60,
        left: 40,
        width: 522
    };

    pdf.fromHTML(
            source, // HTML string
            margins.left, // x coord
            margins.top, {// y coord
                'width': margins.width, // max width of content on PDF
                'elementHandlers': specialElementHandlers
            },
    function (dispose) {
        pdf.save('Results.pdf');
    }, margins);
}

document.getElementById('back').addEventListener("click", function () {
    window.location = "/manageEvents";
});