/*
 @author: Nick Mariani
 @team: stackqueue
 @date: 10/29/15
 @class: onEvent.js
 */

var socket = io("http://localhost:3000");
var id;
var numPar;
var numLanes;
var moreRaces = true;
var doneRacing = 0;
var timerID;
var timers = 0;
var notRaced = [];
var raced = [];
var timerRacers = []; //This for getting the participants for the timer.

if (window.location.href.indexOf("eventId=") > -1) {
    var url = document.URL;
    id = url.substring(url.lastIndexOf("=") + 1); // get the event id from the url
}

$.post('/getTimerId', {eventId: id}, function (data) {
    if (data.done !== false) {
        timerID = data.timerID;
        numPar = data.total_par;
        numLanes = data.num_lanes;
        var n = noty(
                {
                    layout: 'topRight',
                    theme: 'relax',
                    type: 'information',
                    text: 'TimerId: ' + data.timerID,
                    animation:
                            {
                                open: 'animated fadeInRightBig',
                                close: 'animated fadeOutLeft'
                            },
                    maxVisible: 5,
                    closeWith: [],
                    timeout: false, //don't close
                    buttons: false
                });
    }
});

/**
 *  genererates the race table on onEvent.html
 *  
 * @param {type} raceData is an array containing all the 
 * lane number, participant's name and elapsed time from the server
 * @param {type} index
 * @returns {undefined}
 */
function generateTable(raceData, index) {
    var table = document.getElementById('table_body');
    var row = table.insertRow(index);
    var cellLane = row.insertCell(0);
    var cellName = row.insertCell(1);
    var cellTime = row.insertCell(2);
    cellTime.id = 'elapsedTime' + index;
    cellLane.innerHTML = index + 1;
    cellName.innerHTML = raceData;
    cellTime.innerHTML = "-";
}

/**
 * makeRaces
 * 
 * @returns {undefined}
 */
function makeRaces() {
    if (moreRaces) {
        var diff = numPar - doneRacing;
        if (diff > timers) {
            doneRacing += timers;
            getParticipants(timers);
        }
        else
        {
            moreRaces = false;
            doneRacing += diff;
            getParticipants(diff);
        }
    }
}

/**
 * Assigns new participants to timers.
 * 
 * @param {type} racers description
 * @returns {undefined}
 */
function assignNewParticipants(racers) {
    socket.emit("newParticipants",{racers: racers, eventID: id, timerID: timerID});
}

function getParticipants(numRacing) {
    $.post('/getParticipants', {index: id}, function (data) {
        if (data.done !== false) {
            if (notRaced.length === 0) {
                notRaced = data.results;
            }
            timerRacers = [];
            for (var i = 0; i < numRacing; i++) {
                var racers = 0;
                racers = notRaced[0].first_name + " " + notRaced[0].last_name;
                if ($.inArray(racers, raced) === -1) {
                    raced.push(racers);
                    timerRacers.push(racers);
                    notRaced.splice(0, 1);
                    generateTable(racers, i);
                }
            }
            assignNewParticipants(timerRacers);
        }
        else
        {
            var n = noty(
                    {
                        layout: 'topRight',
                        theme: 'relax',
                        type: 'information',
                        text: 'An error occured trying to grab particpants.',
                        animation:
                                {
                                    open: 'animated fadeInRightBig',
                                    close: 'animated fadeOutLeft'
                                },
                        maxVisible: 5,
                        closeWith: ['click'],
                        timeout: 2500, //2500ms
                        buttons: false
                    });
        }
    });
}

///**
// * 
// * @param {type} data
// * @param {type} numRacing
// * @returns {undefined}
// */
//function setParTimer(data, numRacing) {
//    for (var i = 0; i < numRacing; i++) {
//        setTimerPost(data.results[i], i);
//    }
//}
//
///**
// * 
// * @param {type} data
// * @param {type} index
// * @returns {undefined}
// */
//function setTimerPost(data, index) {
//    $.post('/setParticipantsTimer', {id: data.id}, function (done) {
//        if (done.done !== false) {
//            var racers = 0;
//            console.log(data.first_name);
//            racers = data.first_name + " " + data.last_name;
//            generateTable(racers, index);
//        } else {
//            var n = noty(
//                    {
//                        layout: 'topRight',
//                        theme: 'relax',
//                        type: 'information',
//                        text: 'An error occured letting participants into a race.',
//                        animation:
//                                {
//                                    open: 'animated fadeInRightBig',
//                                    close: 'animated fadeOutLeft'
//                                },
//                        maxVisible: 5,
//                        closeWith: ['click'],
//                        timeout: 2500, //2500ms
//                        buttons: false
//                    });
//        }
//    });
//}

document.getElementById('next_race').addEventListener("click", function () {
    $("#table_body").empty();
    makeRaces();
});

document.getElementById('finish').addEventListener("click", function () {
    window.location = "/resultsHTML?eventId=" + id;
});

socket.on("eventJoined", function (data) {
    if (data.eventId == id && data.timerId == timerID) {
        addTimer();
    }
});

socket.on('calculateElapsed', function(data)
{
   if(data.eventId === id && data.timerId === timerID)
   {
       console.log("Beep: " + data.name + " " + data.startTime + " " + data.stopTime);
       for(var i = 0; i < timerRacers.length; i++)
       {
           var racer = timerRacers[i];
           console.log(racer);
           if(racer == data.name)
           {
               var cellTime = document.getElementById('elapsedTime' + i);
               console.log(cellTime);
               var elapsedTime = toHHMMSSdotFF(data.stopTime - data.startTime)
               cellTime.innerHTML = elapsedTime;
               console.log(toHHMMSSdotFF(data.stopTime - data.startTime));
               $.post('/giveElapsedTime', {name: data.name, elapsedTime: elapsedTime, eventID : id},function(data){
                   if(data.done === true) {
                       //do nothing
                   } else {
                       var n = noty(
                    {
                        layout: 'topRight',
                        theme: 'relax',
                        type: 'information',
                        text: 'An error occured updating elapsed time.',
                        animation:
                                {
                                    open: 'animated fadeInRightBig',
                                    close: 'animated fadeOutLeft'
                                },
                        maxVisible: 5,
                        closeWith: ['click'],
                        timeout: 2500, //2500ms
                        buttons: false
                    });
                   }
               });
           }
       }
   }
});

function addTimer() {
    timers++;
}