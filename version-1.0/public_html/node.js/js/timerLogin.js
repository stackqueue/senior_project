/**
 * Check to see if the timer login code exits and log the timer into the event.
 * 
 * @author Richard Cerone
 * @team stackqueue
 * @date 11/12/2015
 */

var socket = io("http://localhost:3000");

/**
 * Checks the server to see if the event code exists for an event. If so,
 * have the user go to the timer page. Otherwise tell the user to try again.
 * 
 * @returns {Boolean} cleared is true if the code matches, else the code doesn't match.
 */
function joinEvent()
{
    var eventCode = document.getElementById("code").value;

    socket.emit("joinEvent", {timerId: eventCode});
}

socket.on("eventJoined", function (data)
{
    if (data.joined === true)
    {
        //Create cookie, empty strings are for potential future values
        var cookie = [data.eventId, data.timerNumber, data.timerId,"",""]; //Add data to cookie.
        var expires = getExpDate(0, 0, 30); //Expire time for cookie.
        setCookie("timerCookie", cookie, expires); //Create cookie.

        window.location = "/timer"; //Redirect to timer.
    }
    else
    {
        var n = noty(
                {
                    layout: 'topRight',
                    theme: 'relax',
                    type: 'error',
                    text: "Event is full.",
                    animation:
                            {
                                open: 'animated fadeInRightBig',
                                close: 'animated fadeOutLeft'
                            },
                    maxVisible: 5,
                    closeWith: ['click'],
                    timeout: 2500, //2500ms
                    buttons: false
                });
    }
});

var timerLogin = document.getElementById("timerLogin");

if (timerLogin.addEventListener)
{
    timerLogin.addEventListener("click", joinEvent, false);
}


