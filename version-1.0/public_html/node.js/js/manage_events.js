/* 
 @author: Nick Mariani
 @team: stackqueue
 @date: 10/29/15
 @class: manage_events.js
 */

/**
 *  retrieves the all event from the server and generates the table
 *  
 * @param {type} param1
 * @param {type} param2
 * @param {type} param3
 */
$.post('/getEvents', {}, function (data) {
    console.log(data.events);
    generateTable(data.events);
});

/**
 *  redirects to viewCreateEvent.html 
 *  
 * @returns {undefined}
 */
function goToCreate() {
    window.location = "/create";
}

/**
 *  genererates the event table on manage_events.html
 *  
 * @param {type} eventList is an array containing all the events from the server
 * @returns {undefined}
 */
function generateTable(eventList) {
    var table = document.getElementById('manageTable');
    for (var i = 0; i < eventList.length; i++) {
        var row = table.insertRow(i + 1);
        var cell = row.insertCell(0);
        cell.innerHTML = eventList[i].event_name + '<br>';
        cell.innerHTML += eventList[i].date.substring(0, 10) + "<br>";
        cell.innerHTML += eventList[i].time + '<br>';
        //each event has three buttons the edit, delete and start event 
        cell.innerHTML += "<button type='button' id='edit'" + i + " onclick='edit(" + eventList[i].id + ")' class='btn btn-warning'>View/Edit Event</button>" +
                "<button type='button' id='delete'" + i + " onclick='deleteEv(" + eventList[i].id + ")' class='btn btn-danger'>Delete Event</button>" +
                "<button type='button' id='start'" + i + " onclick='start(" + eventList[i].id + ")' class='btn btn-success'>Start Event</button>";
    }
}

/**
 * 
 * @param {type} event
 * @returns {undefined}
 */
function saveData(event) {
    var data = [event[0].event_name, event[0].total_par, event[0].num_lanes, event[0].date, event[0].time];
    var exp = getExpDate(0, 0, 5);
    setCookie("event", data, exp);
}

/**
 * 
 * @param {type} participants
 * @returns {undefined}
 */
function savePar(participants) {
    var data = [];
    for (var i = 0; i < participants.length; i++) {
            data.push(participants[i].first_name + " " + participants[i].last_name);
    }
    var exp = getExpDate(0, 0, 5);
    setCookie("participants", data, exp);
}

/**
 *  gets the eventDetails from the server and puts the data 
 *  into a cookie in order to populate the fields onViewCreate.html 
 *  
 * @param {type} index is the id of this event
 * @returns {undefined}
 */
function edit(index) {
    $.post('/editEvent', {indexID: index}, function (data) {
        if (data.done !== false) {
            saveData(data.events);
            $.post('/getParticipants', {index: index}, function (data2) {
                if (data2.done !== false) {
                    savePar(data2.results);
                    window.location = "/create?eventId=" + index;
                }
                else {

                }
            });
        }
        else {
            var n = noty(
                    {
                        layout: 'topRight',
                        theme: 'relax',
                        type: 'error',
                        text: 'Could not edit event',
                        animation:
                                {
                                    open: 'animated fadeInRightBig',
                                    close: 'animated fadeOutLeft'
                                },
                        maxVisible: 5,
                        closeWith: ['click'],
                        timeout: 2500, //2500ms
                        buttons: false
                    });
        }
    });
}

/**
 *  deletes the corresponding event from the database
 *  
 * @param {type} row is the id of the event that will be removed
 * @returns {undefined}
 */
function deleteEv(row) {
    $.post('/delEvent', {rowID: row}, function (data) {
        if (data.done === "deleted") {

            window.location.reload();// = "/manageEvents";
        }
        else if (data.done === "noDelete") {
            var n = noty(
                    {
                        layout: 'topRight',
                        theme: 'relax',
                        type: 'error',
                        text: 'Could not delete',
                        animation:
                                {
                                    open: 'animated fadeInRightBig',
                                    close: 'animated fadeOutLeft'
                                },
                        maxVisible: 5,
                        closeWith: ['click'],
                        timeout: 2500, //2500ms
                        buttons: false
                    });
        }
        else {
            var n = noty(
                    {
                        layout: 'topRight',
                        theme: 'relax',
                        type: 'error',
                        text: 'Unknown error',
                        animation:
                                {
                                    open: 'animated fadeInRightBig',
                                    close: 'animated fadeOutLeft'
                                },
                        maxVisible: 5,
                        closeWith: ['click'],
                        timeout: 2500, //2500ms
                        buttons: false
                    });
        }
    });
}

/**
 *  starts the corresponding event
 *  
 * @param {type} index is the id of this  event
 * @returns {undefined}
 */
function start(index) {
    $.post('/startEvent', {eventID: index}, function (data) {
        if (data.check === "startable") {
            for (var i = 0; i < 3; i++) {
                var timerID = randomString(5);
                $.post('/setTimerID', {timerID: timerID, eventID: index}, function (data2) {
                    if (data2.check === "start") {
                        window.location = '/onEventGUI?eventId=' + index;
                    }
                });
            }
        }
        else if (data.check === "unstartable") {
            var n = noty(
                    {
                        layout: 'topRight',
                        theme: 'relax',
                        type: 'error',
                        text: 'Error: Event not startable',
                        animation:
                                {
                                    open: 'animated fadeInRightBig',
                                    close: 'animated fadeOutLeft'
                                },
                        maxVisible: 5,
                        closeWith: ['click'],
                        timeout: 2500, //2500ms
                        buttons: false
                    });
        }
        else {
            var n = noty(
                    {
                        layout: 'topRight',
                        theme: 'relax',
                        type: 'error',
                        text: 'Error: Unknown Error has occured, could not start event',
                        animation:
                                {
                                    open: 'animated fadeInRightBig',
                                    close: 'animated fadeOutLeft'
                                },
                        maxVisible: 5,
                        closeWith: ['click'],
                        timeout: 2500, //2500ms
                        buttons: false
                    });
        }
    });
}

// assigns the goToCreate() function to this button when clicked
document.getElementById('createEvent').addEventListener("click", function () {
    goToCreate();
});