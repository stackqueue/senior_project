/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function logout()
{
    $.post('/destroySession', {}, function (data)
    {
        if (data.destroyed === true) //Success.
        {
            window.location = "/";
        }
        else
        {
            var n = noty(
                    {
                        layout: 'topRight',
                        theme: 'relax',
                        type: 'error',
                        text: 'Could not logout.',
                        animation:
                                {
                                    open: 'animated fadeInRightBig',
                                    close: 'animated fadeOutLeft'
                                },
                        maxVisible: 5,
                        closeWith: ['click'],
                        timeout: 2500, //2500ms
                        buttons: false
                    });
        }
    });
}

document.getElementById('logout').addEventListener("click", function () {
    logout();
});