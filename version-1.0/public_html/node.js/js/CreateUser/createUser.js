/**
 *Creates a new user and adds them to the database.
 *
 *@author Richard Cerone
 *@group stackqueue
 *@date 10/29/2015
 */

/**
 * create grabs the data from the html document createUser. Then, the password
 * is encrypted via SHA-256 and then sent via AJAX as a JSON array containing 
 * the username and hashed password. If the data returned back is true we will
 * redirect back to the homepage. If the data returned back is false, then we 
 * tell the user the username already exists.
 * 
 * @returns {Boolean, String} True if the the user was added successfully. False
 * if the user already exists and 'error' if there was an internal error with 
 * the server.
 */
function create()
{
    //Get data from createUser.html:
    var username = document.getElementById('newUser').value;
    var password = document.getElementById('userPassword').value;
    
    //Check if username is <= 32 or not 0 and the password >= 8 and not > 32.
    if(username.length !== 0 && username.length <= 32 && password.length >= 8 &&
            password.length <= 32)
    {
        password = sha(password); //Encrypt password via SHA-256.

        //Send AJAX call to server.js with JSON array containing user data.
        $.post("/createUser", {user: username, password: password}, function(data)
        {
            if(data.done === true) //User added successfully.
           {
               //Redirect to homepage with Boolean true.
               window.location = "/?success=true";
           }
           else if(data.done === false) //Username already exists.
           {
               //Use noty to tell user to try a new username.
               var n = noty(
               {
                   layout: 'topRight',
                   theme: 'relax',
                   type: 'error',
                   text: 'Username already exists. Please try another one.',
                   animation:
                   {
                       open: 'animated fadeInRightBig',
                       close: 'animated fadeOutLeft'
                   },
                   maxVisible: 5,
                   closeWith: ['click'],
                   timeout: 2500,    //2500ms
                   buttons: false
               });
           }
           else //Return error.
           {
               console.log("eternal error.");
               //User noty to tell user of eternal error.
               var n = noty(
               {
                   layout: 'topRight',
                   theme: 'relax',
                   type: 'error',
                   text: 'Critical eternal error.',
                   animation:
                   {
                       open: 'animated fadeInRightBig',
                       close: 'animated fadeOutLeft'
                   },
                   maxVisible: 5,
                   closeWith: ['click'],
                   timeout: 2500,    //2500ms
                   buttons: false
               });
           }
        });

    }
    else if(username.length > 32)
    {
        //Tell user the username cannot be more than 32 characters.
        var n = noty(
               {
                   layout: 'topRight',
                   theme: 'relax',
                   type: 'error',
                   text: 'Username cannot be more than 32 characters.',
                   animation:
                   {
                       open: 'animated fadeInRightBig',
                       close: 'animated fadeOutLeft'
                   },
                   maxVisible: 5,
                   closeWith: ['click'],
                   timeout: 2500,    //2500ms
                   buttons: false
               });
    }
    else if(password.length > 32)
    {
        //Tell user the password cannot be more than 32 characters.
        var n = noty(
               {
                   layout: 'topRight',
                   theme: 'relax',
                   type: 'error',
                   text: 'Password cannot be more than 32 characters',
                   animation:
                   {
                       open: 'animated fadeInRightBig',
                       close: 'animated fadeOutLeft'
                   },
                   maxVisible: 5,
                   closeWith: ['click'],
                   timeout: 2500,    //2500ms
                   buttons: false
               });
    }
    else //No response from server. Internal error.
    {
        var n = noty(
               {
                   layout: 'topRight',
                   theme: 'relax',
                   type: 'error',
                   text: 'No response from the server.',
                   animation:
                   {
                       open: 'animated fadeInRightBig',
                       close: 'animated fadeOutLeft'
                   },
                   maxVisible: 5,
                   closeWith: ['click'],
                   timeout: 2500,    //2500ms
                   buttons: false
               });
    }
}

var button = document.getElementById('create');

if(button.addEventListener)
{
    button.addEventListener('click', create, false);
}