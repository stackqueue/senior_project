/**
 * When the user creation is successful, it will return a boolean variable to 
 * the login page of Event Timer. If the boolean variable is equal to true then
 * we will have noty give the user a success notification and to begin to login.
 * 
 * @author Richard Cerone
 * @group stackqueue
 * @date 11/03/2015
 */

//On load we will check if there is a success value in URL
var urlVal = getURLValue('success'); 

/*If not, this okay because that means that we didn't create a new user.
 * In this case the function should return false.
 */
if(urlVal !== false)
{
    //Tell the user creation was successful.
    var n = noty(
    {
        layout: 'topRight',
        theme: 'relax',
        type: 'success',
        text: 'Account created successfully! Try logging in!',
        animation:
        {
            open: 'animated fadeInRightBig',
            close: 'animated fadeOutLeft'
        },
        maxVisible: 5,
        closeWith: ['click'],
        timeout: 2500,    //2500ms
        buttons: false
    });
}
