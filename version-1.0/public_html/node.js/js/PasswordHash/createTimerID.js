/* 
 @author: Shaan Menon
 @team: stackqueue
 @date: 11/07/15
 @class:  timeConverter.js
 */
function randomString(len) {
    charSet =  '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var randomString = '';
    for (var i = 0; i < len; i++) {
    	var randomPoz = Math.floor(Math.random() * charSet.length);
    	randomString += charSet.substring(randomPoz,randomPoz+1);
    }
    //debating on whether to hash this for the db
    return randomString;
}