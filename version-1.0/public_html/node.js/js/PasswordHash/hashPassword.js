/**
 * Hashes password to store into the database. The hash is SHA256. Once a 
 * password is hashed it cannot be undone.
 * 
 * @author Richard Cerone
 * @date 10/29/2015
 * @group stackqueue
 */

/**
 * Simply hashes a password using SHA-256.
 * 
 * @param {String} password to be hashed.
 * @returns {String, Boolean} String of hashed password.
 * Boolean true if password is empty.
 */
function sha(password)
{   
    //Check for empty fields.
    if(password === '')
    {
        var error = true;
        return error;
    }
    else
    {
        var shaObj = new jsSHA("SHA-256", 'TEXT');
        shaObj.update(password.toString());
        var hash = shaObj.getHash('HEX');
        
        return hash;
    }
}

