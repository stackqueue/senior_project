/* 
 @author: Shaan Menon
 @team: stackqueue
 @date: 11/07/15
 @class:  timeConverter.js
 */

/**
 *  Converts given digits to a length of 2 if they arent already
 * @param {String} d, the digits checked
 * @returns {String} two digit converted string
 */
function twoDigits(d) {
    if (0 <= d && d < 10)
        return "0" + d.toString();
    if (-10 < d && d < 0)
        return "-0" + (-1 * d).toString();
    return d.toString();
}

/**
 *  Converts given digits to length of 3 if it isnt already
 * @param {type} d, the digits checked
 * @returns {String} three digit converted string
 */
function threeDigits(d){
    if(0 <= d && d <10)
        return "00" + d.toString();
     if (-10 < d && d < 0)
        return "-00" + (-1 * d).toString();
    if(d < 100)
        return "0" + d.toString();
    return d.toString();
}

/**
 *  Converts a javascript datetime string to a mysql datetime
 * @returns {String} date string converted
 */
Date.prototype.toMySQLFormat = function () {
    return this.getUTCFullYear() + "-" + twoDigits(1 + this.getUTCMonth()) + "-" + twoDigits(this.getUTCDate()) + " " + twoDigits(this.getUTCHours()) + ":" + twoDigits(this.getUTCMinutes()) + ":" + twoDigits(this.getUTCSeconds()) + "." + threeDigits(this.getUTCMilliseconds());
};

function toHHMMSSdotFF(time) {
    var ms = time % 1000;
    time = Math.floor(time/1000);
    var sec = time % 60;
    time = Math.floor(time/60);
    var min = time % 60;
    time = Math.floor(time/60);
    var hours = time;
    return hours + ":" + ensureTwoDigits(min) + ":" + ensureTwoDigits(sec) + "." + ensureThreeDigits(ms);
}

function ensureTwoDigits(time) {
    return time.length === 2 ? time : "0" + time;
}

function ensureThreeDigits(time) {
    time = String(time);
    return time.length > 2 ? time : time.length > 1 ? "0" + time : "00" + time;
}